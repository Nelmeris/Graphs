//
//  my_rand.h
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#pragma once

#ifndef my_rand_h
#define my_rand_h

#include <stdbool.h>

/// Генерация положительного числа с указанной границей и содержанием нуля по желанию
unsigned long my_rand(int range, bool is_included, bool is_zero_included);

#endif /* my_rand_h */
