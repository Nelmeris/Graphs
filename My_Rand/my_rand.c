//
//  my_rand.c
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdlib.h>

#include "my_rand.h"

// Генерация положительного числа с указанной границей и содержанием нуля по желанию
unsigned long my_rand(int range, bool is_included, bool is_zero_included)
{
    if (is_included)
        range++;
    
    if (!is_zero_included)
        range--;
    
    short deviation = (is_zero_included) ? 0 : 1;
    
    return (unsigned long)labs(random() % range + deviation);
}
