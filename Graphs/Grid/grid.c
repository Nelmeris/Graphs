//
//  grid.c
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>

#include "grid.h"
#include "../Vertex/Vertex_Label/vertex_label.h"

// Печать таблицы отношений
void grid_print(graph _graph)
{
    vertex_list_element* _vertex_list_element = _graph.vertices->tail;
    for (int i = 0; i < (_graph.vertices->count * 3) / 2 + 1 - 7; i++)
        printf("-");
    printf("RELATIONS-GRID");
    for (int i = 0; i < (_graph.vertices->count * 3) / 2 + 1 - 7; i++)
        printf("-");
    printf("\n\n");
    // Печать горизонтальной линии обозначений
    printf("  | ");
    for (int i = 0; i < _graph.vertices->count; i++)
    {
        if (_graph.vertices->count <= 52)
            printf("%c  ", _vertex_list_element->value->label);
        else
            printf("%d  ", _vertex_list_element->value->id);
        _vertex_list_element = _vertex_list_element->next;
    }
    printf("\n");
    for (int i = 0; i < _graph.vertices->count * 3 + 2; i++)
    {
        printf("-");
    }
    printf("\n");
    _vertex_list_element = _graph.vertices->tail;
    for (int i = 0; i < _graph.vertices->count; i++)
    {
        // Печать вертикальной линии обозначений
        if (_graph.vertices->count <= 52)
            printf("%c | ", _vertex_list_element->value->label);
        else
            printf("%d | ", _vertex_list_element->value->id);
        // Печать таблицы отношений
        connections_list _connections_list = _vertex_list_element->value->connections;
        vertex_list_element* _vertex_element = _graph.vertices->tail;
        for (int j = 0; j < _graph.vertices->count; j++)
        {
            for (int l = 0; l < _connections_list.count; l++)
                if (_vertex_element->value == _connections_list.list[l].vertex && (_connections_list.list[l].type == doubly || _connections_list.list[l].type == simply_out))
                {
                    printf("%d  ", _connections_list.list[l].heft);
                    break;
                }
                else if (l + 1 == _connections_list.count || _connections_list.list[l].type == simply_in)
                {
                    printf("0  ");
                    break;
                }
            _vertex_element = _vertex_element->next;
        }
        printf("\n  |\n");
        _vertex_list_element = _vertex_list_element->next;
    }
    printf("\n");
}
