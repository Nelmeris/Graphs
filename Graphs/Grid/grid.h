//
//  grid.h
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#pragma once

#ifndef grid_h
#define grid_h

#include "../graph.h"

/// Печать матрицы отношений
void grid_print(graph graph);

#endif /* grid_h */
