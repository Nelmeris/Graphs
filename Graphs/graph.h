//
//  graph.h
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#ifndef graph_h
#define graph_h

typedef enum {
    doubly, simply_in, simply_out
} connection_type;

/// Связь
typedef struct {
    struct vertex* vertex; // Связуемая вершина
    int heft; // Вес связи
    connection_type type; // Тип связи: 1 - двусторонняя, 2 - в сторону родителя, 3 - в сторону новой вершины
} connection;

/// Список связей
typedef struct {
    connection* list; // Связи
    int count; // Количество связей
} connections_list;





/// Вершина
typedef struct vertex {
    int id; // Идентификатор
    char label; // Имя
    
    connections_list connections; // Список связей
} vertex;




/// Элемент списка вершин
typedef struct vertex_list_element {
    vertex* value;
    
    struct vertex_list_element* next;
    struct vertex_list_element* prev;
} vertex_list_element;



/// Список вершин
typedef struct {
    vertex_list_element* head;
    vertex_list_element* tail;
    
    int count;
} vertex_list;




/// Граф
typedef struct {
    vertex_list* vertices; // Список вершин
} graph;

/// Печать графа
void graph_print(graph graph);

/// Создание графа
graph graph_create(int vertices_lenght);

#endif /* graph_h */
