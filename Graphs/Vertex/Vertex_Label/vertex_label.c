//
//  vertex_label.c
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include "vertex_label.h"

// Генерация обозначаемого символа вершины
// (ограничение для корректной работы - 52 символа)
char get_label(int deviation)
{
    return (deviation < 26) ? 'A' + deviation : 'a' + deviation - 26;
}
