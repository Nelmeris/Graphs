//
//  vertex_label.h
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#ifndef vertex_label_h
#define vertex_label_h

/// Генерация обозначаемого символа вершины
char get_label(int deviation);
// (ограничение для корректной работы - 52 символа)

#endif /* vertex_label_h */
