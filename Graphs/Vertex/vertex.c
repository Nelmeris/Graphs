//
//  vertex.c
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>

#include "graph.h"
#include "grid.h"
#include "../Connection/connection.h"
#include "../Vertex/Vertex_List/vertex_list.h"
#include "Vertex_Label/vertex_label.h"

// Создание вершины
void vertex_create(graph* graph, connections_list connections)
{
    vertex* _vertex = malloc(sizeof(vertex));
    
    _vertex->id = graph->vertices->count;
    _vertex->label = get_label(graph->vertices->count);
    
    // Создание связей
    _vertex->connections = connections;
    if (connections.count > 0)
        vertex_connections_bind(_vertex, connections);
    
    if (_vertex->id % 1000 / 100 * 100 == _vertex->id % 1000)
        printf("%d\n", _vertex->id);
    
    vertex_list_push_element(graph->vertices, _vertex);
}
