//
//  vertex.h
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#ifndef vertex_h
#define vertex_h

#include "../graph.h"

/// Создание вершины
void vertex_create(graph* graph, connections_list connections);

#endif /* vertex_h */
