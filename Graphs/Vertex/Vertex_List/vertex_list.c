//
//  vertex_list.c
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdlib.h>

#include "../../graph.h"

void vertex_list_push_element(vertex_list* list, vertex* element)
{
    vertex_list_element* list_element = malloc(sizeof(vertex_list_element));
    list_element->value = element;
    list_element->next = NULL;
    
    if (list->count != 0)
    {
        list_element->prev = list->head;
        list->head->next = list_element;
    } else {
        list_element->prev = NULL;
        list->tail = list_element;
    }
    list->head = list_element;
    list->count++;
}

vertex* vertex_list_get_element(vertex_list* list, int index)
{
    vertex_list_element* list_element = list->tail;
    if (list_element->value->id == index)
        return list_element->value;
    
    for (int i = 0; i < list->count - 1; i++)
    {
        list_element = list_element->next;
        if (list_element->value->id == index)
            return list_element->value;
    }
    
    return NULL;
}
