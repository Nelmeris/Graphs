//
//  vertex_list.h
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#ifndef vertexList_h
#define vertexList_h

#include "../../graph.h"

void vertex_list_push_element(vertex_list* list, vertex* element);

vertex* vertex_list_get_element(vertex_list* list, int index);

#endif /* vertex_list_h */
