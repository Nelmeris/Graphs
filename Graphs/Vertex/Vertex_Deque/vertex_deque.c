//
//  vertex_deque.c
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>

#include "vertex_deque.h"

// Добавление в начало очереди элемента
void vertex_deque_push_front(vertex_deque* deque, vertex* value)
{
    vertex_deque_element* element = malloc(sizeof(vertex_deque_element));
    element->value = value;
    element->next = NULL;
    
    if (deque->size != 0)
    {
        element->prev = deque->head;
        deque->head->next = element;
    } else {
        element->prev = NULL;
        deque->tail = element;
    }
    deque->head = element;
    deque->size++;
}

// Добавление в конец очереди элемента
void vertex_deque_push_back(vertex_deque* deque, vertex* value)
{
    vertex_deque_element* element = malloc(sizeof(vertex_deque_element));
    element->value = value;
    element->prev = NULL;
    
    if (deque->size != 0)
    {
        element->next = deque->tail;
        deque->tail->prev = element;
    } else {
        element->next = NULL;
        deque->head = element;
    }
    deque->tail = element;
    deque->size++;
}

// Взятие из начала очереди элемента
vertex* vertex_deque_pop_front(vertex_deque* deque)
{
    if (deque->size == 0) // Если очередь пуста
    {
        printf("deque is empty");
        vertex* vertex = NULL;
        return vertex;
    }
    vertex_deque_element* next = deque->head;
    deque->head = deque->head->prev;
    deque->size--;
    vertex* value = next->value;
    free(next);
    return value;
}

// Взятие из конца очереди элемента
vertex* vertex_deque_pop_back(vertex_deque* deque)
{
    if (deque->size == 0) // Если очередь пуста
    {
        printf("deque is empty");
        vertex* vertex = NULL;
        return vertex;
    }
    vertex_deque_element* next = deque->tail;
    deque->tail = deque->tail->next;
    vertex* value = next->value;
    free(next);
    deque->size--;
    return value;
}
