//
//  vertex_deque.h
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#pragma once

#ifndef deque_h
#define deque_h

#include "../../graph.h"

/// Элемент двусторонней очереди (вершина)
typedef struct vertex_deque_element
{
    vertex* value;
    
    struct vertex_deque_element* next;
    struct vertex_deque_element* prev;
} vertex_deque_element;

/// Двусторонная очередь (вершины)
typedef struct vertex_deque
{
    vertex_deque_element* tail;
    vertex_deque_element* head;
    
    int size;
} vertex_deque;

/// Добавление вершины в начало очереди
void vertex_deque_push_front(vertex_deque* deque, vertex* value);
/// Добавление вершины в конец очереди
void vertex_deque_push_back(vertex_deque* deque, vertex* value);

/// Взятие вершины из начала очереди
vertex* vertex_deque_pop_front(vertex_deque* deque);
/// Взятие вершины из конца очереди
vertex* vertex_deque_pop_back(vertex_deque* deque);

#endif /* vertex_deque_h */
