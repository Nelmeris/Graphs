//
//  connection.c
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdlib.h>

#include "graph.h"
#include "my_rand.h"
#include "../Vertex/Vertex_List/vertex_list.h"

// Создание обратной связи с новой вершиной
void vertex_connection_bind(vertex* vert, connection _connection, short connection_type)
{
    // Создание временного списка соединений
    connection* temp_connections = calloc(_connection.vertex->connections.count + 1, sizeof(connection));
    for (int i = 0; i < _connection.vertex->connections.count; i++)
        temp_connections[i] = _connection.vertex->connections.list[i];
    
    // Добавление новой связи
    temp_connections[_connection.vertex->connections.count].vertex = vert;
    temp_connections[_connection.vertex->connections.count].heft = _connection.heft;
    temp_connections[_connection.vertex->connections.count].type = connection_type;
    
    // Очистка памяти
    free(_connection.vertex->connections.list);
    
    // Перенос списка
    _connection.vertex->connections.list = temp_connections;
    _connection.vertex->connections.count++;
}

// Связывание вершины с другими вершинами по списку
void vertex_connections_bind(vertex* vertex, connections_list connections)
{
    for (int i = 0; i < connections.count; i++)
    {
        short connectionType = 0;
        switch (connections.list[i].type) {
            case doubly:
                connectionType = doubly;
                break;
            case simply_out:
                connectionType = simply_in;
                break;
            case simply_in:
                connectionType = simply_out;
                break;
        }
        vertex_connection_bind(vertex, connections.list[i], connectionType);
    }
}

// Создание рандомных связей для новой вершины
connections_list vertex_connections_create(graph graph)
{
    // Создание списка соединений максимально возможного размера
    connections_list connections;
    connections.list = calloc(graph.vertices->count, sizeof(connection));
    connections.count = 0;
    
    // Цикл прохождения по всем созданным вершинам и создание связи с рандомной вероятностью
    for (int j = 0; j < graph.vertices->count; j++)
    {
        if (my_rand(2, true, false) == 1) // Вероятность связанности
        {
            connection connection;
            connection.vertex = vertex_list_get_element(graph.vertices, j); // Связываемая вершина
            connection.type = (short)my_rand(3, false, true); // Случайный тип связи
            connection.heft = (int)my_rand(9, true, false); // Рандомный вес связи
            connections.list[connections.count++] = connection;
        }
    }
    
    // Корректировка размера списка соединений
    connection* connections_final = calloc(connections.count, sizeof(connection));
    for (int i = 0; i < connections.count; i++)
        connections_final[i] = connections.list[i];
    
    // Очистка памяти
    free(connections.list);
    connections.list = connections_final;
    connections_final = NULL;
    
    return connections;
}
