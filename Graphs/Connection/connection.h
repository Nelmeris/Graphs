//
//  connection.h
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#ifndef connection_h
#define connection_h

#include "../graph.h"

/// Связывание вершин
void vertex_connections_bind(vertex* vertex, connections_list connections);

/// Создание списка вершин
connections_list vertex_connections_create(graph graph);

#endif /* connection_h */
