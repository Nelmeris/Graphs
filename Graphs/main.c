//
//  main.c
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include "graph.h"
#include "Grid/grid.h"
#include "Bypass/bypass_width.h"

/// Количество вершин графа
#define ELEMENTS 26

int main(int argc, const char * argv[]) {
    graph graph = graph_create(ELEMENTS);
    
    graph_print(graph);
    grid_print(graph);
    bypass_width(graph);
    
    return 0;
}
