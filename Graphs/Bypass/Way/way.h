//
//  way.h
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#ifndef way_h
#define way_h

#include "../../graph.h"

typedef struct {
    char end_point_label;
    char starting_point_label;
    
    int end_point_id;
    int starting_point_id;
} bypass_way;

/// Сохранение путя, полученного во время обхода
void way_save(bypass_way* way, vertex vertex_start, vertex vertex_end);

/// Печать всех путей, полученных во время обхода
void ways_print(bypass_way* ways, int count);

#endif /* way_h */
