//
//  way.c
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdio.h>

#include "way.h"

void way_save(bypass_way* way, vertex vertex_start, vertex vertex_end)
{
    way->starting_point_id = vertex_start.id;
    way->starting_point_label = vertex_start.label;
    way->end_point_id = vertex_end.id;
    way->end_point_label = vertex_end.label;
}

void ways_print(bypass_way* ways, int count)
{
    printf("-----BYPASS-WIDTH-WAYS-----\n");
    for (int i = 0; i < count; i++)
        if (count <= 52)
            printf("%c --> %c\n", ways[i].starting_point_label, ways[i].end_point_label);
        else
            printf("%d --> %d\n", ways[i].starting_point_id, ways[i].end_point_id);
}
