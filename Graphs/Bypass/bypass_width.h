//
//  bypass_width.h
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#pragma once

#ifndef bypass_width_h
#define bypass_width_h

#include "../graph.h"

/// Обход графа в ширину
void bypass_width(graph graph);

#endif /* bypass_width_h */
