//
//  bypass_width.c
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "bypass_width.h"
#include "../Vertex/Vertex_Deque/vertex_deque.h"
#include "../Vertex/Vertex_List/vertex_list.h"
#include "Condition/condition.h"
#include "Way/way.h"

vertex_deque deque;

// Обход в ширину
void bypass_width(graph graph)
{
    condition* conditions = calloc(graph.vertices->count, sizeof(condition)); // Список состояний
    for (int i = 0; i < graph.vertices->count; i++)
    {
        conditions[i] = not_passed;
        vertex_deque_push_back(&deque, vertex_list_get_element(graph.vertices, i)); // Добавление вершин в конец очереди
    }
    
    bypass_way* ways = calloc(graph.vertices->count, sizeof(bypass_way));
    int ways_count = 0;
    
    vertex* current_vertex;
    
    printf("--------------------BYPASS-WIDTH-CONDITIONS--------------------\n");
    conditions_print(conditions, graph.vertices->count);
    do // Цикл, оканчивающий по прохождению всех вершин (passed)
    {
        current_vertex = vertex_deque_pop_front(&deque); // Взятие вершины из начала очереди
        if (conditions[current_vertex->id] != 3) // Проверка на пройденность вершины
        {
            int count = 0; // Счетчик прочитанных связанных вершин
            for (int i = current_vertex->connections.count - 1; i >= 0; i--) // Обратный поход по связанным вершинам
                if (current_vertex->connections.list[i].type != simply_in) // Проверка на тип связи (simplyIn - входящая)
                {
                    if (conditions[current_vertex->connections.list[i].vertex->id] == not_passed) // Проверка на непрочитанность вершины
                    {
                        way_save(&ways[ways_count++], *current_vertex, *current_vertex->connections.list[i].vertex); // Сохранение найденного путя
                        conditions[current_vertex->connections.list[i].vertex->id] = read; // Установка вершине статуса прочитанной
                        vertex_deque_push_front(&deque, vertex_list_get_element(graph.vertices, current_vertex->connections.list[i].vertex->id)); // Добавление связанной вершины в начало очереди
                    }
                    count++;
                    conditions_print(conditions, graph.vertices->count);
                }
            /*
             Если из вершины не было путей (count = 0) или она не была прочитанна, то статус не изменяется
             */
            if (count > 0 || vertex_list_get_element(graph.vertices, current_vertex->id)->connections.count == 0 || conditions[current_vertex->id] == read)
                conditions[current_vertex->id] = passed;
        }
    } while (!conditions_check(conditions, graph.vertices->count));
    
    printf("\n");
    ways_print(ways, ways_count);
}
