//
//  condition.h
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#ifndef condition_h
#define condition_h

#include <stdbool.h>

/// Состояния графа
typedef enum {
    not_passed, read, passed
} condition;

/// Проверка завершения всех состояний
bool conditions_check(condition* conditions, int count);

/// Печать состояний
void conditions_print(condition* conditions, int count);

#endif /* condition_h */
