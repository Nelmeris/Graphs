//
//  condition.c
//  Graphs
//
//  Created by Artem Kufaev on 04/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdio.h>

#include "condition.h"

/// Проверка всех состояний на прочтение
bool conditions_check(condition* conditions, int count)
{
    for (int i = 0; i < count; i++)
        if (conditions[i] != passed)
            return false;
    return true;
}

void conditions_print(condition* conditions, int count)
{
    for (int i = 0; i < count; i++) // Вывод состояний вершин
        printf("%d ", conditions[i]);
    printf("\n");
}
