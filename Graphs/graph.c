//
//  graph.c
//  Graphs
//
//  Created by Artem Kufaev on 02/12/2018.
//  Copyright © 2018 Artem Kufaev. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>

#include "graph.h"
#include "Connection/connection.h"
#include "Vertex/vertex.h"
#include "Vertex/Vertex_List/vertex_list.h"
#include "Vertex/Vertex_Label/vertex_label.h"

// Печать графа
void graph_print(graph graph)
{
    printf("--------------------CREATED-GRAPH--------------------\n");
    for (int i = 0; i < graph.vertices->count; i++)
    {
        vertex* _vertex = vertex_list_get_element(graph.vertices, i);
        if (graph.vertices->count <= 52)
            printf("%c ", _vertex->label);
        else
            printf("%d ", _vertex->id);
        printf("%p \n", &*_vertex); // Печать адреса вершины
        for (int j = 0; j < _vertex->connections.count; j++) // Печать связей
        {
            // Печать типа связи
            switch (_vertex->connections.list[j].type)
            {
                case doubly:
                    printf("<-->");
                    break;
                    
                case simply_out:
                    printf("--->");
                    break;
                    
                case simply_in:
                    printf("<---");
                    break;
            }
            if (graph.vertices->count <= 52) // Печать символа связи
                printf(" %c ", _vertex->connections.list[j].vertex->label);
            else
                printf(" %d ", _vertex->connections.list[j].vertex->id);
            printf(" %p \n", &(*_vertex->connections.list[j].vertex).id); // Печать адреса связи
        }
        printf("\n");
    }
}

graph graph_create(int vertices_count)
{
    graph graph;
    
    graph.vertices = malloc(sizeof(vertex_list));
    graph.vertices->count = 0;
    
    // Создание графа
    for (int i = 0; i < vertices_count; i++)
        vertex_create(&graph, vertex_connections_create(graph));
    
    return graph;
}
